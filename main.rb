#!/usr/bin/env ruby

# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

class Report
  def run
    report_data = []

    Dir.glob('**/*.qml') do |file|
      next if file.include?('test')
      next if file.include?('example')

      data = File.read(file)
      skip_next = false
      data.lines.each_with_index do |line, i|
        if skip_next
          skip_next = false
          next
        end
        skip_next =  line.include?('// nolint')
        report_data << [file, i, line] if line.match(/^\s*text\s*:\s*"[^"]+"/)
      end
    end

    report_data.each do |file, number, line|
      puts "#{file}:#{number}: #{line}"
    end
  end
end

exit Report.new.run.empty? if __FILE__ == $0
