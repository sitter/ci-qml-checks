# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2024 Harald Sitter <sitter@kde.org>

require_relative 'test_helper'

class TemplateTest < Minitest::Test
  def test_foo
    File.write('/tmp/foo.qml', <<-FOO)
    text: "foo"
  text  :  "foo"    //   shitty one
  text  :  "foo"    /*   other shitty one */
text
text: i18nc("context", "text")
      text: foo
    text: {
* text: "foo"
 //   text  :  "foo"
 // nolint
 text: "bad"
    FOO

    Dir.chdir('/tmp')
    refute system("#{__dir__}/../main.rb")

    assert_equal(3, Report.new.run.size)
  end
end
